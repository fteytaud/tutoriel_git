---
title: "Tutoriel git <small><br> <hr> [introduction](index.html) | [installation](installation.html) | [dépôt local](depot_local.html) | [dépôt distant](depot_distant.html) | [branches](branches.html) | [forks](forks.html) <hr> </small>"
output:
    toc: true
---

* * * * *

# Dépôt local (journaliser un projet)

L'élément de base d'un projet git est le dépôt. Il s'agit simplement d'un
dossier classique que l'on demande à git de versionner.

Un dépôt git contient l'ensemble des commits (versions sauvegardées du projet).
Ces commits peuvent être marqués par des étiquettes. Il existe deux étiquettes
prédéfinies et gérées par git : `master` (dernier commit) et `HEAD` (commit
courant, correspondant à l'état courant des fichiers).

## Initialiser un dépôt

Allez dans le dossier à versionner puis lancez la commande `git init`. 

![](depot_local_01.png)

Git créé alors un dossier `.git` pour y stocker ses metadonnées.  Si vous ne
voulez plus versionner votre dossier, il suffit de supprimer ce dossier.  Le
reste du temps, ne touchez pas à ce dossier si vous n'êtes pas absolument
sûr(e) de ce que vous faites.

## Afficher l'état d'un dépôt

La commande `git status` affiche l'état courant du dépôt : nouveaux fichiers,
fichiers modifiés, fichiers supprimés.  Cette commande est très sûre; elle ne
peut rien casser.  N'hésitez donc pas à la lancer pour voir où vous en êtes.

![](depot_local_02.png)

## Ajouter des fichiers dans le dépôt

Il faut indiquer explicitement les fichiers que git doit journaliser.  

Par exemple, imaginons qu'on ouvre un nouveau fichier `rapport_01.tex` dans
notre projet.

![](depot_local_03.png)

On y écrit du code latex.

![](depot_local_04.png)

Et on affiche l'état du dépôt.

![](depot_local_05.png)

Git nous indique que le fichier `rapport_01.tex` existe bien dans le dossier
(ainsi qu'un fichier de sauvegarde), mais qu'il ne fait pas partie du dépôt. 

Pour ajouter un fichier au dépôt, il faut utiliser la commande `git add`. 

![](depot_local_06.png)

Ceci indique que git doit prendre en compte le fichier lors des prochains
commits (journalisation), ce qu'on peut vérifier avec un `git status`.

![](depot_local_07.png)

![](depot_local_07.svg)

## Valider les modifications (commit)

Après avoir ajouté, modifié ou supprimé des fichiers, on peut valider les
modifications, c'est-à-dire sauvegarder un état du projet (commit). Encore une
fois, seuls les fichiers qui ont été ajoutés au projet (avec `git add`) sont
pris en compte. 

La commande à lancer pour valider toutes les modifications est `git commit -a`.

![](depot_local_08.png)

Git lance alors l'éditeur de texte pour le message de commit.
Le message est pré-rempli avec des informations concernant le commit. Ces
informations vous permettent de vérifier que le commit correspond bien à ce que
vous voulez mais ne feront pas partie du message final.

Écrivez un message expliquant rapidement les modifications que vous êtes en
train de valider puis enregistrez et quittez. 

![](depot_local_09.png)

Git créé alors le nouveau commit, qui valide ici le fichier `rapport_01.tex`.

![](depot_local_10.png)

Vous pouvez le vérifier avec un `git status`.

![](depot_local_11.png)

![](depot_local_11.svg)

Ainsi, un `git commit` crée un commit à partir de l'état pointé par HEAD
et fait pointer master (et HEAD) sur ce nouveau commit.

## Fichiers autogénérés et .gitignore

Git est très pratique pour gérer des fichiers textes que l'on modifie
régulièrement. En revanche, il n'est pas adapté pour gérer des fichiers dans un
format non-textuel (images, données compressées, documents word...).  Si on
ajoute un tel fichier dans le dépôt alors à chaque modification, git stockera
une nouvelle copie complète (au lieu de stocker uniquement les lignes qui ont
changé), ce qui est inefficace.

D'autre part, il est inutile de versionner les fichiers autogénérés (créés
automatiquement lors de la compilation du code). Par exemple, si on compile le
code latex de notre projet...

![](depot_local_12.png)

... des fichiers intermédiaires sont créés et apparaissent lors des `git status` :

![](depot_local_13.png)

Pour les ignorer, il suffit d'ajouter un fichier `.gitignore` et d'y indiquer
les fichiers à ignorer.

![](depot_local_14.png)

Ainsi, il n'apparaissent plus lors des `git status`.

![](depot_local_15.png)

![](depot_local_15.svg)

Il est de pratique courante d'ajouter le `.gitignore` au dépôt, ainsi qu'un
fichier `README.md` qui servira de page d'accueil (au format
[markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)).

![](depot_local_16.png)

![](depot_local_16.svg)


## Renommer et supprimer des fichiers

La commande `git mv` permet de renommer un fichier versionné. Il ne faut pas
utiliser la commande système (`mv`) car dans ce cas git ne saura pas qu'il faut
renommer le fichier (il pensera que le fichier manque et essaiera de le
recréer).

![](depot_local_17.png)

![](depot_local_17.svg)

De même, la commande `git rm` permet de supprimer un fichier versionné.

![](depot_local_18.png)

![](depot_local_18.svg)


## Afficher l'historique des commits

Faisons d'abord une modification "commitée" puis une autre non commitée.

![](depot_local_19.png)

La commande `git log` permet d'afficher l'historique des commits.

![](depot_local_20.png)

On peut également le voir avec un client graphique graphique comme `gitg`.

![](depot_local_21.png)

Le "unstaged" correspond aux modifications courantes non commitées.

![](depot_local_log.svg)

## Supprimer les modifications courantes

La commande `git checkout -- . ` supprime les modifications courantes. Le dépôt revient
alors dans l'état du dernier commit (master).

![](depot_local_checkout_01.svg)

## Revenir à un commit précédent (sans suppression)

La commande `git checkout 7b3...` permet de revenir au commit "7b3...". On passe 
ainsi dans un mode "détaché" (HEAD ne pointe plus sur un commit terminal).

![](depot_local_checkout_02.svg)

Au lieu d'utiliser le numéro de commit, on peut également utiliser les
étiquettes. Par exemple, `git checkout master` pour aller au dernier commit
principal ou `git checkout HEAD~1` pour aller au commit précédent le commit
courant.

## Marquer un commit

La commande `git tag` permet de nommer des commits pour, par exemple, fixer une
version ou pouvoir revenir plus facilement sur un commit particulier.

![](depot_local_22.png)

![](depot_local_checkout_03.svg)

## Supprimer des commits précédents

La commande `git reset 073...` revient au commit "073..." et annule tous les
commits suivant (jusqu'à l'ancien master). Les modifications correspondants à
ces commits sont conservées dans l'état courant (non commité).

![](depot_local_checkout_04.svg)

Autre possibilité : la commande `git reset --hard 073...` revient au commit
"073..." et annule tous les commits suivant **en supprimant les modifications
correspondantes**.

![](depot_local_checkout_05.svg)

## Résumé et méthode de travail

Résumé des commandes git précédentes :

---|---|
`git init` | initialise le dossier courant (nouveau dépôt git) |
`git status` | TODO |
`git add` | |
`git commit -a` | |
`git mv` | |
`git rm` | |
`git log` | |
`git checkout -- .` | |
`git checkout <commit/etiquette>` | |
`git tag` | |
`git reset` | |
`git reset --hard` | |

Quelques conseils de méthode de travail :

- privilégier les petits dépôts, correspondant à différents travaux, plutôt
  qu'un gros dépôt regroupant tous les travaux de l'année
- faire des commits réguliers et correspondant aux petites tâches successives 
- éviter de commiter un projet cassé (par exemple, du code qui ne compile pas)
- ne pas versionner les fichiers auto-générés

## Exercice


TODO

* * * * *

[Retour au début de la page](#header)

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-03-05

