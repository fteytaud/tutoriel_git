---
title: "Tutoriel git <small><br> <hr> [introduction](index.html) | [installation](installation.html) | [dépôt local](depot_local.html) | [dépôt distant](depot_distant.html) | [branches](branches.html) | [forks](forks.html) <hr> </small>"
output:
    toc: true
---

* * * * *

# Branches (travailler en équipe)

TODO

## Afficher les branches 
git log --graph --all --oneline --decorate

## Créer une nouvelle branche locale

## Changer de branche

## Fusionner des branches

## Envoyer une branche sur un dépôt distant

## Afficher les branches distantes

## Terminer une branche locale

## Terminer une branche distante

## Supprimer un commit déjà pushé (DANGER !!!)
git reset --hard e6c8e8e
git push origin HEAD:master -f
git reset --hard e6c8e8e

![](branches_01.png)
![](branches_02.png)
![](branches_03.png)
![](branches_04.png)
![](branches_05.png)
![](branches_06.png)
![](branches_07.png)
![](branches_08.png)
![](branches_09.png)
![](branches_10.png)
![](branches_11.png)
![](branches_12.png)
![](branches_13.png)
![](branches_14.png)
![](branches_15.png)
![](branches_16.png)
![](branches_17.png)
![](branches_18.png)
![](branches_19.png)
![](branches_20.png)
![](branches_21.png)

## Résumé et méthode de travail

Résumé des commandes git précédentes :

---|---|
`git ` | |

Quelques conseils de méthode de travail :

- TODO


## Exercice

TODO

* * * * *

[Retour au début de la page](#header)

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-03-05

