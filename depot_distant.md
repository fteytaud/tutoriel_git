---
title: "Tutoriel git <small><br> <hr> [introduction](index.html) | [installation](installation.html) | [dépôt local](depot_local.html) | [dépôt distant](depot_distant.html) | [branches](branches.html) | [forks](forks.html) <hr> </small>"
output:
    toc: true
---

* * * * *

# Dépôt distant (sauvegarder un projet sur un serveur)

Git permet de synchroniser un dépôt local avec un dépôt distant (sur un
serveur).  Ceci permet d'envoyer les commits locaux sur le serveur et de
récupérer les commits du serveur dans le dépôt local. 
Lorsqu'un dépôt local est synchronisé avec un serveur, une étiquette
supplémentaire (`origin/master`) est utilisée pour indiquer le dernier commit
du serveur.

Il existe des serveurs comme github qui permettent d'héberger
gratuitement des dépôts publics (visibles par tout le monde).
Le serveur gogs du SCOSI vous permet d'héberger des dépôts publics ou privés.
Il dispose d'une page web vous permettant de gérer vos projets. Pour cela,
allez à l'adresse
[https://gogs.univ-littoral.fr](https://gogs.univ-littoral.fr) et entrez vos
identifiants du portail ULCO :

![](depot_distant_01.png)

Une fois identifié(e), le site vous affiche une page d'accueil (derniers
commits, dépôts actifs...) :

![](depot_distant_02.png)

## Créer un dépôt sur un serveur git

Pour créer un nouveau dépôt distant, allez sur la page du serveur gogs et
cliquez "New Repository".

![](depot_distant_03.png)

Entrez le nom du dépôt distant à créer (évitez les espaces), puis cliquez
"Create Repository".

![](depot_distant_04.png)

Le dépôt distant est alors créé et une page vous indique comment le récupérer
localement. Attention, il y a une méthode plus simple que celle indiquée (cf
section suivante).

![](depot_distant_05.png)

## Cloner un dépôt distant vers un nouveau dépôt local

La commande `git clone ...` permet de récupérer un dépôt distant sur votre
machine (cf image ci-dessous). Pensez à mettre votre login après le "https://"
(pour vous éviter d'avoir à le retaper sans arrêt, plus tard).

![](depot_distant_06.png)

## Synchroniser un dépôt local existant vers un dépôt distant

Si vous avez déjà créé et modifié un dépôt local, 

![](depot_distant_07a.png)

![](depot_distant_07a.svg)

alors vous pouvez le synchroniser
avec un dépôt distant en utilisant les commandes `git remote add origin ...`
puis `git push -u origin master`. 

![](depot_distant_07b.png)

![](depot_distant_07b.svg)

Si vous avez le choix, faites plutôt un clone (méthode de la section
précédente) : c'est plus simple et le résultat sera exactement le même.

## Récupérer (tirer) les commits d'un dépôt distant

La commande `git pull` permet de récupérer les éventuelles modifications sur le
serveur et de les intégrer dans votre dépôt local.

![](depot_distant_08.png)

![](depot_distant_08.svg)

## Envoyer (pousser) les commits locaux sur un dépôt distant

Après des commits locaux...

![](depot_distant_09a.png)

![](depot_distant_09a.svg)

... la commande `git push` permet d'envoyer vos commits locaux sur le serveur.

![](depot_distant_09b.png)

![](depot_distant_09b.svg)

Les commits/fichiers envoyés sur le serveur sont alors visibles sur la page web.

![](depot_distant_10.png)

Vous pouvez également voir le contenu de votre dépôt avec un client
graphique.

![](depot_distant_11.png)

## Résoudre des conflits

Imaginez que vous avez modifié et commité un fichier, et que vous voulez
l'envoyer sur le serveur. Pour cela, il faut d'abord faire un `git pull` avant
le `git push` au cas où l'un de vos collaborateurs aurait envoyer une
modification sur le serveur entre temps. 

Si des modifications ont été faites en parallèle...

![](depot_distant_12a.svg)

... git est souvent capable de les fusionner mais parfois il n'arrive pas à
résoudre le conflit et il vous demande de le faire manuellement.

![](depot_distant_12.png)

![](depot_distant_12b.svg)

Pour résoudre un conflit, il suffit d'ouvrir le fichier en cause et de
remplacez les zones marquées par le contenu que vous voulez obtenir. 
Il existe des outils graphiques comme `meld` qui peuvent vous y aider.

![](depot_distant_13.png)

Une fois les conflits édités, vous pouvez commiter...

![](depot_distant_14a.png)

![](depot_distant_14a.svg)

... et pusher vers le serveur.

![](depot_distant_14b.png)

![](depot_distant_14b.svg)

## Résumé et méthode de travail

Résumé des commandes git précédentes :

---|---|
`git clone` | récupère un dépôt distant |
`git pull` | récupère les modifications du dépôt distant et les intègre dans le dépôt local |
`git push` | envoie les commits du dépôt local sur le dépôt distant |

Quelques conseils de méthode de travail :

- utilisez la page
  [https://gogs.univ-littoral.fr](https://gogs.univ-littoral.fr) pour créer et
gérer vos dépôts distants.
- récupérez vos nouveaux dépôts distant avec `git clone`
- récupérez les modifications du serveur avec `git pull`
- faites un `git pull` avant d'envoyer vos nouveaux commits sur le serveur (`git push`)

## Exercice

TODO

* * * * *

[Retour au début de la page](#header)

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-03-05

