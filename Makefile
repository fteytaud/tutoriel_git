MD_FILES = $(shell find -name "*.md")
HTML_FILES = $(MD_FILES:.md=.html)

all: $(HTML_FILES)

%.html: %.md
	pandoc -s --toc -H style.html -o $@ $<

publish:
	scp $(HTML_FILES) *.svg *.png yangra.univ-littoral.fr:public-html/enseignements/tutoriel_git/

clean:
	rm -f $(HTML_FILES)

