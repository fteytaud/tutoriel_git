---
title: "Tutoriel git <small><br> <hr> [introduction](index.html) | [installation](installation.html) | [dépôt local](depot_local.html) | [dépôt distant](depot_distant.html) | [branches](branches.html) | [forks](forks.html) <hr> </small>"
output:
    toc: true
---

* * * * *

# Installation et configuration

Pour utiliser git, il vous faut une connexion internet et un client git.

## Installation sous Linux

La plupart des systèmes Linux modernes propose le client git dans leur système
de paquets. Par exemple, sur les Linux basés Debian, il suffit de lancer la
commande console :

```
sudo apt-get install git
```

Il existe également des clients graphiques : gitg, giggle, qgit, gitk, git-gui...

## Installation sous Windows ou Mac

Télécharger et installer le client [git pour
Windows](http://git-scm.com/download/win) ou le client [git pour
Mac](http://git-scm.com/download/mac).

## Configuration

Après avoir installé le client git, il faut configurer le nom et l'adresse
e-mail de l'utilisateur; par exemple, en tapant les commandes consoles : 

```
git config --global user.name "Julien Dehos"
git config --global user.email dehos@nimpe.org
```

On peut également configurer l'éditeur de texte et le proxy que le client git
doit utiliser; par exemple, en tapant les commandes consoles :

```
git config --global core.editor emacs
git config --global http.proxy http://user:pass@proxyhost:proxyport
```
ou bien en ajoutant des variables d'environnement (par exemple, dans le
`.bashrc`): 
```
export EDITOR=emacs
export http_proxy=http://user:pass@proxyhost:proxyport
```

## Obtenir de l'aide

Aide générale sur git :
```
git help 
```

Aide sur une commande git :
```
git help <commande git>
```

Voir également la section [références](index.html#références).

## Exercice

Installez et configurez le client git sur votre machine.  Vérifiez que votre
client git peut accéder à l'extérieur, par exemple en récupérant le dépôt
distant `https://github.com/juliendehos/invinoveritas` :

![](installation_01.png)

* * * * *

[Retour au début de la page](#header)

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-03-05

