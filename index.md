---
title: "Tutoriel git <small><br> <hr> [introduction](index.html) | [installation](installation.html) | [dépôt local](depot_local.html) | [dépôt distant](depot_distant.html) | [branches](branches.html) | [forks](forks.html) <hr> </small>"
output:
    toc: true
---

* * * * *

# Introduction

## À qui s'adresse ce tutoriel ?
- Objectif du tutoriel : apprendre à utiliser l'outil git et le [serveur
  gogs](https://gogs.univ-littoral.fr) mis en place par le
[SCOSI](http://scosi.univ-littoral.fr/). 
- Pré-requis : utilisation basique d'un ordinateur (et notamment du clavier).
- Public visé : les développeurs au sens large (code latex, python, matlab, R,
  java...) qui souhaitent journaliser et/ou partager du code.

## Qu'est-ce-que git et gogs ?
- git : système décentralisé de journalisation de code source (alternative à
  mercurial, subversion, CVS...)
- gogs : service d'hébergement de code source utilisable avec git (alternative
  à github, bitbucket...)

## Dans quels cas utiliser git/gogs ?
git est conçu pour manipuler principalement des fichiers **au format texte**
(code source, code latex, fichier CSV...). Attention, les fichiers word, excel
et PDF ne sont pas des fichiers au format texte.

Quelques fonctionnalités de git :

- journalisation 
- sauvegarde distante 
- synchronisation 
- travail en équipe 
- projets publics ou privés :

## Dans quels cas ne pas utiliser git/gogs ?
- édition collaborative en temps-réel -> sharelatex
- partage de fichiers -> serveur ftp, http...
- fichiers dans un format non textuel (word, excel, PDF...) -> dropbox

## Concepts de base

## Notion de "version" (commit)

Un "projet git" contient l'historique de toutes les modifications de chaque
fichier.  On appelle "commit" un état sauvegardé de ces modifications (une
version du projet en quelque sorte).

Généralement, on modifie le projet à partir de la version précédente et ansi de
suite; ce qui correspond à une succession de commits :

![](concepts_commits_1.svg)

Mais on peut également effectuer des modifications en parallèles. On appelle ça
des branches :

![](concepts_commits_2.svg)

On peut ensuite rassembler ces modifications en fusionnant les branches :

![](concepts_commits_3.svg)

## Notion de dépôt (repository)

On appelle dépôt l'ensemble des fichiers de notre "projet git", c'est-à-dire
l'historique des commits.

Un dépôt peut être dupliqué sur différentes machines, pour synchroniser les
données ou pour travailler en équipe. 

Dans l'ancien temps des années 2000, on avait un dépôt principal distant que
l'on synchronisait ou modifiait via des copies locales partielles (subversion,
CVS...) :

<img src="concepts_depots_svn.svg" style="width:700px">

Aujourd'hui, on utilise plutôt un système de dépôts décentralisés (git,
mercurial...).  Avec ce système, tous les dépôts sont identiques et peuvent se
synchroniser entre eux :

<img src="concepts_depots_git.svg" style="width:700px">

Généralement, on garde tout de même un dépôt sur un serveur distant accessible
en permanence (serveur gogs, github...).

## Références

- [http://git-scm.com/book/fr/v2](http://git-scm.com/book/fr/v2)
- [https://www.atlassian.com/git/tutorials/](https://www.atlassian.com/git/tutorials/)

* * * * *

[Retour au début de la page](#header)

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-03-05

