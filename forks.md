---
title: "Tutoriel git <small><br> <hr> [introduction](index.html) | [installation](installation.html) | [dépôt local](depot_local.html) | [dépôt distant](depot_distant.html) | [branches](branches.html) | [forks](forks.html) <hr> </small>"
output:
    toc: true
---

* * * * *

# Forks (collaborer avec d'autres projets)

TODO

## Forker un dépôt distant

## Envoyer un pull request

## Résumé et méthode de travail

Résumé des commandes git précédentes :

---|---|
`git ` | |

Quelques conseils de méthode de travail :

- TODO

## Exercice

TODO

* * * * *

[Retour au début de la page](#header)

[Retour à la page d'accueil](../../index.html)

Dernière mise à jour : 2016-03-05

